# A self-hosted wedding registry

## Features

- Easy to host with services like VercelJS or GitLab Pages
- Multilingual by design
- Uses AirTable as a backend database
- Guests can contribute to or buy a specific gift
- Guests can get the couple's bank details to send money
- Guests can get the bank details of a charity chosen by the couple to send money

## Tech stack

- NextJS + React
- AirTable
- Formik + Yup
- Lottie
