import Airtable from 'airtable'
import { Item } from './Components/GiftList'

function getBase() {
  return new Airtable({apiKey: process.env.AIRTABLE_API_KEY}).base('appBhpahI4CDlkZbd')
}

function ItemFromRecord(locale: string, record: any): Item {
  return {
    ...record.fields,
    'id': record.id,
    'image': record.fields['image'] ? record.get('image')[0].thumbnails.large : null,
    'title': record.get(`title-${locale}`) || null,
    'price': record.get(`price-${locale}`) || null,
    'description': record.get(`description-${locale}`) || null,
  }
}

export async function Gifts(locale: string) {
  const base = getBase()
  let fields = [
    'image',
    'link',
    'status',
    `title-${locale}`,
    `description-${locale}`,
    `price-${locale}`
  ]
  
  const p = new Promise<Item[]>((resolve, reject) => {
    base('Wedding list').select({
      // Selecting the first 3 records in Grid view:
      maxRecords: 50,
      view: "Grid view",
      fields,
      filterByFormula: '{visible}'
    }).firstPage(function page(err, records) {
      if (err) { 
        console.error(err); 
        reject(err)
        return; 
      }
      let res = []
      records.forEach(function(record) {
        res.push(ItemFromRecord(locale, record))
      });
      resolve(res)
    })
  })
  return p
}

export async function Gift(locale: string, id: string) {
  const base = getBase()
  const p = new Promise<Item>((resolve, reject) => {
    base('Wedding list').find(id, function(err, record) {
      if (err) { 
        console.error(err); 
        reject(err)
        return; 
      }
      resolve(ItemFromRecord(locale, record))
    })
  })
  return p
}

export async function AddGift(data) {
  const base = getBase()
  return new Promise((resolve, reject) => {
    base('Hozzájárulások').create([
      {
        "fields": {
          "Name": data.name,
          "Mire": [
            data.id
          ],
          "Összeg": data.amount
        }
      }
    ], function(err) {
      if (err) {
        reject(err)
        return
      }
      resolve(true)
    });
  })
}

export async function api<T>(to: string, data?: object): Promise<T> {
  return window.fetch(to, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  .then(response => {
    if(response.status == 200) {
      return response.json()
    }
    throw new Error('Failed generating a name')
  })
}