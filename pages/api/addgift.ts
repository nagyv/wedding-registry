import { AddGift } from '../../api.service'

export default async function handler(req, res) {
  if(req.method !== 'POST') {
    res.statusCode = 405
    res.end()
  }

  const result = await AddGift(req.body)
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify(result))
}
