import React from "react"
import { useRouter } from "next/router"
import { CssBaseline } from "@material-ui/core"
import { IntlProvider } from "react-intl"
import * as locales from "../lang/compiled"
import Footer from "../Components/Footer"

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  const { locale, defaultLocale, pathname } = router
  const messages = locales[locale]

  return <IntlProvider
      locale={locale}
      defaultLocale={defaultLocale}
      messages={messages}
    >
    <CssBaseline />
    <Component {...pageProps} />
    <Footer />
  </IntlProvider>
}

export default MyApp
