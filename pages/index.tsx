import React, { useState } from 'react'
import { Box, Container, makeStyles, Paper, Tab, Tabs, Typography } from '@material-ui/core'
import Head from 'next/head'
import {FormattedMessage} from 'react-intl'
import { useIntl } from "react-intl"
import GiftList, { Item } from '../Components/GiftList'
import { Gifts } from '../api.service'
import Tamogatas from '../Components/StaticContent'
import Hello from '../Components/Hello'
import SendCash from '../Components/SendCash'

const useStyles = makeStyles({
  tabs: {
    flexGrow: 1,
  },
});

const Home: React.FC<{list: Item[]}> = ({list}) => {
  const classes = useStyles();
  const { formatMessage } = useIntl()
  const [value, setValue] = useState([0, 'list']);

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    const options = ['list', 'cash', 'cause']
    setValue([newValue, options[newValue]]);
  };

  const TabContent = {
    'list': <GiftList items={list} />,
    'cash': <SendCash />,
    'cause': <Tamogatas />
  }

  return (
      <Container>
        <Head>
          <title>Gift registry - Ajándéklista</title>
        </Head>
        <Hello />
        <Typography>
        <FormattedMessage
          defaultMessage="<p>Both of us prefers to give, and if you'd ask us what we want as a wedding gift, we'll tell you that we don't want anything, as we have everything that matters.</p>
          <p>This is true now too. We are alive, healthy and surronded by nice people including you. At the same time, as you might know, we've started a big project, the HOME project. 
          While we have many things to finish our house, there are still a few missing items. Each item in the house is choosen and crafted with my care. Every item in this list aim to make the house even more HOME.</p>"
          description="Mindketten elsősorban adni szeretünk és az esetek többségében, ha megkérdezik
          tőlünk, hogy mit kérünk pl. az esküvőre, azt mondjuk, hogy semmit, hiszen
          mindenünk megvan.
          Egyrészről ez továbbra is így van, hiszen egészségesek vagyunk, élünk és
          szeretetteljes közeg vesz minket körül a ti személyetekben. Másrészről, mint ahogy
          mindannyian tudjátok nagy fába vágtuk a fejszénket. Úgy döntöttünk, hogy
          belevágunk a HOME projektbe és ugyan sok mindenünk megvan hozzá, de pár
          dolog még azért hiányzik ahhoz, hogy késznek nyilváníthassuk a közös házunkat. A
          ház minden egyes része gondos kezek munkájával épül újjá, amit a kívánságlistán
          szereplő tételekkel igyekszünk még otthonosabbá tenni."
          values={{
            p: (children) => <Typography>{children}</Typography>
          }}
        />
        </Typography>
        <dl>
          <dt><FormattedMessage
          defaultMessage="Cash"
          description="Cash"
        /></dt>
          <dd><FormattedMessage 
            defaultMessage="We will spend every penny we receive on the below list, in the order they appear on the list"
            description="Explain cash gifts"
          /></dd>
          <dt><FormattedMessage
          defaultMessage="Choose from the list"
          description="Válassz a listából"
        /></dt>
          <dd><FormattedMessage 
            defaultMessage="If there is an item on the list that would remind us about you (or you think it's a must in a real home), then choose this option. 
            If you would like to buy or contribute to a specific gift, we will be happy to take your support."
            description="Ha van olyan tárgy a listán, amiről szeretnéd, ha eszünkbe jutnál mikor ránézünk,
            akkor választ ezt az opciót. ;) A viccet félretéve, ha úgy véled, hogy szívesen
            megvennéd, vagy beleadnál célzottan egy-egy ajándékba, akkor örömmel fogadjuk."
          /></dd>
          <dt><FormattedMessage
          defaultMessage="Support our favourite cause"
          description="Támogasd a kedvenc civil szervezetünket"
        /></dt>
          <dd><FormattedMessage 
            defaultMessage="If you'd like to support an organisation that we like to support too, then pick this option."
            description="Ha egy olyan szervezetet szeretnél támogatni amit mi is, akkor válaszd ezt a
            lehetőséget."
          /></dd>
        </dl>
        <Paper className={classes.tabs}>
          <Tabs
            value={value[0]}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            <Tab label={formatMessage({
              defaultMessage: "Choose from the list",
              description: "Válassz a listából"
            })} />
            <Tab label={formatMessage({
              description: 'Cash',
              defaultMessage: 'Cash'
            })} />
            <Tab label={formatMessage({
              description: 'Supporting a cause',
              defaultMessage: 'Supporting a cause'
            })} />
          </Tabs>
          <Box p={3}>
            {TabContent[value[1]]}
          </Box>
        </Paper>
      </Container>
  )
}

export default Home

export async function getServerSideProps({locale}) {  
  return {
    props: {list: await Gifts(locale)}, // will be passed to the page component as props
  }
}
