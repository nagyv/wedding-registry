import { Button, Container, MenuItem, TextField, Typography } from '@material-ui/core'
import React, { useState } from 'react'
import { Gift } from '../api.service'
import { Item } from '../Components/GiftList'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import { api } from '../api.service'
import GiftDetails from '../Components/GiftDetails'
import Thanks from '../Components/Thanks'
import { FormattedMessage } from 'react-intl'
import { useRouter } from 'next/router'

const GiftSchema = Yup.object().shape({
  name: Yup.string()
    .min(3, 'Too Short!')
    .max(100, 'Too Long!')
    .required('Required'),
  amount: Yup.number()
    .min(1)
    .required('Required'),
  currency: Yup.string().oneOf(['HUF', 'USD', 'EUR']).required('Required'),
})

const exchangeRates = {
  'HUF': 1,
  'USD': 293.04,
  'EUR': 359.47
}

const GiftPage: React.FC<{locale: string, gift: Item}> = ({locale, gift}) => {
  const [_gift, setGift] = useState(gift)
  const [hasContributed, setContributed] = useState(false)
  const router = useRouter()
  const {full} = router.query

  const formik = useFormik({
    initialValues: {
      name: '',
      amount: full ? gift.price - gift.status : 0,
      currency: full ? full : 'HUF'
    },
    validationSchema: GiftSchema,
    onSubmit: async (values, {setSubmitting}) => {
      await api('/api/addgift', {
        ...values,
        amount: exchangeRates[values.currency as string] * values.amount,
        id: gift.id
      })
      setSubmitting(false)
      const newGift: Item = await api('/api/getgift', {
        id: gift.id,
        locale
      })
      setContributed(true)
      setGift(newGift)
    },
  });

  return <Container>
    <GiftDetails gift={_gift} />
    {hasContributed 
      ? <Thanks />
      : <React.Fragment>
          <Typography variant="h4">
            <FormattedMessage 
              description="Your details"
              defaultMessage="Your details"
            />
          </Typography>
          <form onSubmit={formik.handleSubmit}>
            <TextField
              fullWidth
              id="name"
              name="name"
              label="Your name so we can say thank you"
              value={formik.values.name}
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
            />
            <TextField
              select
              variant="outlined"
              id="currency"
              name="currency"
              value={formik.values.currency}
              onChange={formik.handleChange}
              error={formik.touched.currency && Boolean(formik.errors.currency)}
              helperText={formik.touched.currency && formik.errors.currency}
            >
              <MenuItem value='HUF'>Ft</MenuItem>
              <MenuItem value='USD'>$</MenuItem>
              <MenuItem value='EUR'>€</MenuItem>
            </TextField>
            <TextField
              variant="outlined"
              id="amount"
              name="amount"
              type="number"
              value={formik.values.amount}
              onChange={formik.handleChange}
              error={formik.touched.amount && Boolean(formik.errors.amount)}
              helperText={formik.touched.amount && formik.errors.amount}
            />
            <Button variant="contained" fullWidth color="primary" disabled={formik.isSubmitting} type="submit">Send</Button>
          </form>
    </React.Fragment>
    }
  </Container>
}

export default GiftPage

export async function getServerSideProps({locale, params}) {
  return {
    props: {
      locale,
      gift: await Gift(locale, params.gift)
    }, // will be passed to the page component as props
  }
}

