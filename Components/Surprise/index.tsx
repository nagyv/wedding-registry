import { Box, makeStyles } from '@material-ui/core'
import React from 'react'
import Lottie from 'react-lottie'
import animationData from './40721-surprise-in-a-gift-box.json'

const useStyle = makeStyles({
  root: {
    position: 'absolute',
    height: '70px',
    width: '50px',
    top: '20px',
    left: '20px'
  }
})

const Surprise = () => {
  const classes = useStyle()
  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

    return <Box className={classes.root}><Lottie 
    options={defaultOptions} 
    height={70}
    width={50}
    /></Box>
}

export default Surprise