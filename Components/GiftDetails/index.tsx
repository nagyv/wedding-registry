import { Grid, LinearProgress, makeStyles, Typography } from "@material-ui/core"
import React from "react"
import { FormattedMessage, FormattedNumber } from "react-intl"
import { BikaBank, NikiBank } from "../BankDetails"
import { Item } from '../GiftList'
import Surprise from "../Surprise"

const useStyle = makeStyles({
  h1: {
    paddingLeft: '50px'
  }
})

const GiftDetails: React.FC<{gift: Item}> = ({gift}) => {
  const classes = useStyle()
  return <React.Fragment>
    <Surprise />
    <Typography variant="h1" className={classes.h1}>{gift.title}</Typography>
    <Typography variant="subtitle1">
      <FormattedMessage 
        description="Ajándékozás aloldal címe"
        defaultMessage="Support us to get started together"
      />
    </Typography>
    <img src={gift.image.url} height={gift.image.height} width={gift.image.widht} />
    <Typography>{gift.description}</Typography>
    <Typography variant="h3">
      <FormattedMessage 
        description="Jelenleg mennyi pénz jött be"
        defaultMessage="Current funding"
      />
    </Typography>
    <Typography>
      <FormattedNumber 
        style='unit'
        unit='percent'
        value={gift.status / gift.price * 100}
      />
    </Typography>
    <LinearProgress variant="determinate" value={gift.status / gift.price * 100} />
    <Typography variant="h3">
      <FormattedMessage 
        description="A Te hozzájárulásod"
        defaultMessage="Your contribution"
      />
    </Typography>
    <Typography>
      <FormattedMessage 
        description="Contribution instructions"
        defaultMessage="Please send your contribution to Niki or Viktor and register it here, so we can say thank you, and others know the current funding"
      />
    </Typography>
    <Grid container>
      <Grid item xs={12} sm={6}>
        <Typography variant="h4">
          <FormattedMessage 
            description="Niki's bank details"
            defaultMessage="Niki's bank details"
          />
        </Typography>
        <NikiBank />
      </Grid>
      <Grid item xs={6}>
        <Typography variant="h4">
          <FormattedMessage 
            description="Viktor's bank details"
            defaultMessage="Viktor's bank details"
          />
        </Typography>
        <BikaBank />
      </Grid>
    </Grid>
  </React.Fragment>
}

export default GiftDetails