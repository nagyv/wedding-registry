import { Box, makeStyles } from '@material-ui/core';
import Lottie from 'react-lottie'
import animationData from './31779-hello-love.json'

const useStyles = makeStyles({
  root: {
    margin: '30px auto'
  },
});

const Hello = () => {
  const classes = useStyles()
  const defaultOptions = {
    loop: false,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

    return <Box className={classes.root}><Lottie 
    options={defaultOptions} 
    height={150}
    width={150}
    />
    </Box>
}

export default Hello