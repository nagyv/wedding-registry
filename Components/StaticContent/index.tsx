import { Card, CardContent, CardHeader, Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { FontanusBank, WSBBank } from '../BankDetails'

const useStyles = makeStyles({
  h3: {
    padding: '2rem 0px'
  }
})

const Tamogatas: React.FC = () => {
  const classes = useStyles()
  return <React.Fragment>
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h2">
          <FormattedMessage
            defaultMessage="Support one of our favourite NGO"
            description="Támogasd a kedvenc civil szervezetünket" />
        </Typography>      
        <Typography>
          <FormattedMessage
            defaultMessage="If you would like to support an organisation together with us, then choose this option"
            description="Ha egy olyan szervezetet szeretnél támogatni amit mi is, akkor válaszd ezt a
    lehetőséget." />
        </Typography>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h3" className={classes.h3}>
          <FormattedMessage
            defaultMessage="Fontanus Foundation"
            description="Fontanus Alapítvány" />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography>
          <FormattedMessage
            defaultMessage="We have learned a lot about life and ourselves while having a log of fun at the Fontanus Academy. As a result, we try to support their work whenever we can."
            description="A Fontanus Akadémián nagyon sokat tanultunk az életről és magunkról és nagyon
            sok örömteli percet szerzett nekünk az Akadémia, így minden lehetőséget
            megragadunk, hogy támogassuk az Fontanus tevékenységét." />
        </Typography>
        <br/>
        <Typography>
          <FormattedMessage
            defaultMessage="Fun fact: we've met at the Fontanus Academy"
            description="Fun fact: A Fontanus Akadémián ismerkedtünk meg." />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Card>
          <CardHeader title="Bank details"></CardHeader>
          <CardContent>
            <FontanusBank />
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h3" className={classes.h3}>
          <FormattedMessage
            defaultMessage="Water Skyball Association"
            description="Water Skyball Egyesület" />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography>
          <FormattedMessage
            defaultMessage="Water Skyball is a very exciting, hungarian sport that both of us practices for many years."
            description="A WSB egy nagyon élvezetes magyar fejlesztésű sportág, amit évek óta mindketten
            játszunk." />
        </Typography>
        <br/>
        <Typography>
          <FormattedMessage
            defaultMessage="Fun fact: Viktor popped the question during the last season."
            description="Fun fact: Viktor a tavalyi szezon alatt kérte meg a kezemet." />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Card>
            <CardHeader title="Bank details"></CardHeader>
            <CardContent>
              <WSBBank />
            </CardContent>
          </Card>
      </Grid>  
    </Grid>
  </React.Fragment>
}

export default Tamogatas
