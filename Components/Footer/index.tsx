import React from "react";
import Link from 'next/link'
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    marginTop: '10px',
    padding: '10px',
    borderTop: '2px solid #ddd',
    textAlign: 'center',
  },
});

const Footer: React.FC = () => {
  const classes = useStyles();
  return <footer className={classes.root}>
    <Link href="https://gitlab.com/nagyv/wedding-registry/-/blob/main/LICENSE">
      <a><Typography component="span" align="center">Licensing information here</Typography></a>
    </Link>
  </footer>
}

export default Footer