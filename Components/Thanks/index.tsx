import Lottie from 'react-lottie'
import animationData from './42564-thanks-button.json'

const Thanks = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true, 
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  return <Lottie 
    options={defaultOptions} 
    height={150}
    width={450}
    />
}

export default Thanks