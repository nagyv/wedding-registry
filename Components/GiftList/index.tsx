import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, LinearProgress, makeStyles, Typography } from '@material-ui/core';
import { useRouter } from 'next/router';
import React from 'react'
import { FormattedMessage, FormattedNumber } from 'react-intl';
import Link from 'next/link'

export interface Item {
  id: string,
  title: string,
  price: number,
  image: {
    url: string,
    height: number,
    widht: number
  },
  link: string,
  status: number,
  description: string,
}

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin: '10px',
  },
  media: {
    height: 140,
  },
});

const GiftList: React.FC<{items: Item[]}> = ({items}) => {
  const classes = useStyles();
  const {locale} = useRouter()
  
  return <Grid container>
    {items.map(item => 
      <Grid key={`item-${item.id}`} item xs={12} sm={3}>
        <Card className={classes.root}>
          <Link
            href={{
              pathname: '/[gift]',
              query: { gift: item.id },
            }}
          >
            <CardActionArea component="a">
              {item.image && <CardMedia
                className={classes.media}
                image={item.image.url}
                title={item.title}
              />}
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  {item.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {item.description}
                </Typography>
                <LinearProgress variant="determinate" value={item.status / item.price * 100} /> <FormattedNumber 
                  style='unit'
                  unit='percent'
                  value={item.status / item.price * 100}
                />
                <br />
                <FormattedNumber value={item.price} style="currency" currency={locale === 'en' ? 'EUR' : 'HUF'} />
              </CardContent>
            </CardActionArea>
          </Link>
          {item.status < item.price 
          ? <CardActions>
            <Link
              href={{
                pathname: '/[gift]',
                query: { gift: item.id },
              }}
              passHref
            >
              <Button component="a" color="primary" variant="contained"><FormattedMessage 
                defaultMessage="Contribute here"
                description="Contribute button"
              /></Button>
            </Link>
            <Link
              href={{
                pathname: '/[gift]',
                query: { gift: item.id, full: locale === 'en' ? 'EUR' : 'HUF' },
              }}
              passHref
            >
              <Button component="a" variant="outlined"><FormattedMessage 
                defaultMessage="Give this to us"
                description="Buy butonn"
              />
              </Button>
            </Link>
            {item.link && <Button size="small" color="primary">
            <FormattedMessage 
                defaultMessage="Learn more"
                description="Learn more buton"
              />
            </Button>}
          </CardActions>
          : <CardActions>
              <Typography align="center">
              <FormattedMessage 
                defaultMessage="Thank you all!"
                description="Köszönjük nektek!"
              />
              </Typography>
            </CardActions>}
        </Card>
      </Grid>
    )}
  </Grid>
}

export default GiftList