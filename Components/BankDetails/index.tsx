import { Typography } from '@material-ui/core'
import React from 'react'
import { FormattedMessage } from 'react-intl'

interface BankData {
  name: string,
  account: string,
  swift: string,
  iban?: string,
  paypal?: string
}

const BankDetails: React.FC<{data: BankData}> = ({data}) => {
  return <Typography>
    <FormattedMessage 
      description="Bank name"
      defaultMessage="Bank: {name}"
      values={data}
    />
    <br />
    <FormattedMessage 
      description="Bank account number"
      defaultMessage="Account number: {account}"
      values={data}
    />
    <br />
    <FormattedMessage 
      description="SWIFT code"
      defaultMessage="SWIFT code: {swift}"
      values={data}
    />
    <br />
    {data.iban && <FormattedMessage 
      description="IBAN number"
      defaultMessage="IBAN: {iban}"
      values={data}
    /> }
    <br />
    {data.paypal &&
      <FormattedMessage 
        description="PayPal address"
        defaultMessage="PayPal address: {paypal}"
        values={data}
      />
    }
  </Typography>
}

const BikaBank = () => {
  return <BankDetails data={{
    name: 'Raiffeisen',
    account: '12010659-01471525-00100006',
    swift: 'UBRTHUHBXXX',
    iban: 'HU96120106590147152500100006',
    paypal: 'viktor.nagy@gmail.com'
  }} />
}

const NikiBank = () => {
  return <BankDetails data={{
    name: 'Budapest Bank',
    account: '10103898-64465200-01000004',
    swift: 'BUDAHUHBXXX',
    iban: 'HU82101038986446520001000004'
  }} />
}

const FontanusBank = () => {
  return <BankDetails data={{
    name: "Budapest Bank",
    account: "10103173-23948500-01005001",
    swift: "BUDAHUHBXXX"
  }} />
}

const WSBBank = () => {
  return <BankDetails data={{
    name: "CIB Bank Zrt.",
    account: "10700062-48307501-51100005",
    swift: "CIBHHUHB"
  }} />
}

export {BikaBank, NikiBank, FontanusBank, WSBBank}
export default BankDetails