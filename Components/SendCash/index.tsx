import { Card, CardContent, CardHeader, Grid, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import { FormattedMessage } from 'react-intl'
import { BikaBank, FontanusBank, NikiBank, WSBBank } from '../BankDetails'

const useStyles = makeStyles({
  h3: {
    padding: '2rem 0px'
  }
})

const SendCash: React.FC = () => {
  const classes = useStyles()
  return <React.Fragment>
    <Grid container>
      <Grid item xs={12}>
        <Typography>
          <FormattedMessage
              defaultMessage="Please send your support to one of our accounts"
              description="Cash intro"
            />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography variant="h4">
          <FormattedMessage 
            description="Niki's bank details"
            defaultMessage="Niki's bank details"
          />
        </Typography>
        <NikiBank />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography variant="h4">
          <FormattedMessage 
            description="Viktor's bank details"
            defaultMessage="Viktor's bank details"
          />
        </Typography>
        <BikaBank />
      </Grid>
    </Grid>
  </React.Fragment>
}

export default SendCash
